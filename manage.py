# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野

from flask import Flask
from flask_cors import CORS

from interface import interfacetest
from project_setting import project_setting
from datetime import timedelta
import os
from flask_session import Session
from login import loginng

session = Session()
app = Flask(__name__)
app.config['SESSION_TYPE'] = 'filesystem'
app.config['SECRET_KEY'] = os.urandom(24)
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(weeks=1)
app.config['JSON_AS_ASCII'] = False
CORS(app, supports_credentials=True)  # 全局结局跨域请求问题

app.register_blueprint(interfacetest)
app.register_blueprint(project_setting)
app.register_blueprint(loginng)

Session(app)


if __name__ == "__main__":
    app.run('0.0.0.0', 5555, debug=True, threaded=True)


