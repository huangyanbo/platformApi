# !/usr/bin/env python
# -*- encoding: utf-8 -*-

import time


def save_case_log(sv, data, db, resp, source=0):
    global log_id
    if source == 0:
        __logname = str(int(time.time())) + '手动执行记录'
        log_id = sv.add_case_log_id(data, __logname, source)
    else:
        __logname = str(int(time.time())) + '定时任务执行记录'


    res = db.test_platform(log_id, m='w')
    if res["code"] == 1:
        sql2 = sv.add_case_response_log(resp, res['id'])
        db.test_platform(sql2, m='w')
    else:
        pass
    return True
