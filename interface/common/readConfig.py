# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野

import configparser
import os
path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
config = configparser.ConfigParser()
config.read(path+'/config/config.ini', encoding='utf-8')

class ReadConfig(object):
    def get_email(self, name):
        value = config.get('EMAIL', name)
        return value

    def get_http(self, name):
        value = config.get('HTTP', name)
        return value

    def get_mysql(self, name):
        value = config.get('DATABASE', name)
        return value

if __name__ == '__main__':  # 测试一下，我们读取配置文件的方法是否可用
    print('HTTP中的baseurl值为：', ReadConfig().get_http('serviceUrl'))
    print('EMAIL中的开关on_off值为：', ReadConfig().get_email('on_off'))