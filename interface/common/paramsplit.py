# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# import json


class Paramsplit:

    def paramsplit(self, corr, results):  # 关联参数拆分
        global resultss, final_corr_dict
        corr = str(corr).replace('\n', '').replace('\r', '').split(";")  # split()分割
        corr_dict = {}  # 最终拆分结果存储在这里
        for j in range(len(corr)):
            param = corr[j].split("=")  # 以=两端拆分-成为列表
            for key in param[1][1:-1].split("]["):  # 拆分=右边的[result][yangli]，
                if isinstance(results, list):  # results是个list时
                    for i in range(len(results)):
                        resultss = eval(str(results[i]))
                else:
                    pass
                if key not in resultss.keys():
                    final_corr_dict = {}
                    return final_corr_dict
                else:
                    temp = resultss[key]  # 从results中取key
                    results = temp  # 转换，！！！
            corr_dict[param[0]] = results  # 拆分结构存入字典
            final_corr_dict = str(corr_dict)  # 将字典类型转换成string类型存储，以便存入case中
        return final_corr_dict

    def checkpointsplit(self, checkpoint):  # 检查点拆分
        checkpoint = str(checkpoint).replace('\n', '').replace('\r', '').split(";")
        return checkpoint

    def casenumsplit(self, casenum):  # 拆分并转换表格编号（用例编号+1）
        casenum = str(casenum).replace('\n', '').replace('\r', '').split(",")
        return casenum


# if __name__ == '__main__':
    # corr = "${planId}=[CARD][planId]"
    # results = {'planId': 320013, 'enabled': True, 'cardName': 'API自动化测试卡', 'shopConfig': [{'shopId': '102417112', 'platShopId': '102417112', 'platCode': 'TAOBAO', 'shopName': '奥康国际官方旗舰店'}], 'omni': False, 'cardCategory': True, 'plugins': [], 'menus': []}

    # Paramsplit().paramsplit(corr, results)
    # objkey = 'memberId'
    # objdict = {'pageSize': 20, 'list': [{'id': 1068030368, 'gradeList': [{'id': 1067940366, 'memberId': 1068030368, 'modified': '2020-11-10 10:55:03'}]}], 'pageNum': 1}
    # Dict_get().dict_get(objkey, objdict)
    # corr = '${memberId}=[list][gradeList][memberId]'
    # results = [{'pageSize': 20, 'totals': 0, 'list': [
    #     {'id': 1068030368, 'cardPlanId': 320013, 'cardNumber': 'TAOBAO自动化数据', 'status': 'NORMAL', 'name': 'TAOBAO自动化数据',
    #      'created': '2020-11-10 10:55:01', 'openPlatCode': 'TAOBAO', 'modified': '2020-11-10 10:55:01',
    #      'mobile': '19909090909', 'mixMobile': '91cb95d490f5277997799f3bf5c7a642', 'openShopId': '102417112',
    #      'tenantId': 'qiushi6', 'mobileModified': '2020-11-10 10:55:01', 'platCode': 'TAOBAO', 'pointList': [
    #         {'id': 1067840366, 'tenantId': 'qiushi6', 'cardPointPlanId': 324405, 'currentValidPoint': 0,
    #          'currentFreezePoint': 0, 'totalGainPoint': 6, 'totalConsumePoint': 6, 'totalOverduePoint': 0,
    #          'modified': '2020-11-10 10:55:03'}], 'gradeList': [
    #         {'id': 1067940366, 'memberId': 1068030368, 'cardPlanId': 320013, 'tenantId': 'qiushi6',
    #          'cardGradePlanId': 324505, 'currentGradeId': 327538, 'currentEffectiveDate': '2020-01-23',
    #          'currentOverdueDate': '2025-12-12', 'modified': '2020-11-10 10:55:03'}]}], 'pageNum': 1}]
    # corr = '${planId}=[CARD][planId]'
    # results = [{'CARD': [{'planId': 320013, 'enabled': True, 'cardName': 'API自动化测试卡', 'shopConfig': [
    #     {'shopId': '102417112', 'platShopId': '102417112', 'platCode': 'TAOBAO', 'shopName': '奥康国际官方旗舰店'}],
    #                       'omni': False, 'cardCategory': True, 'plugins': [], 'menus': []}], 'TAOBAO': [
    #     {'id': 60042, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': '20180903192',
    #      'thirdCaption': '亮亮的小shop', 'unitId': 60042, 'platShopId': '294335825', 'support': True},
    #     {'id': 180052, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_70724306',
    #      'thirdCaption': '奥康皮具店', 'unitId': 180052, 'platShopId': '70724306', 'support': True},
    #     {'id': 10000300819, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_20199001',
    #      'thirdCaption': '人群营销A002', 'unitId': 20000001239, 'platShopId': '20199001', 'support': True},
    #     {'id': 10000300829, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_666660009',
    #      'thirdCaption': '数据平台组测试店铺9', 'unitId': 20000001249, 'platShopId': '666660009', 'support': True},
    #     {'id': 10000305958, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_105765354',
    #      'thirdCaption': '店铺_mock', 'unitId': 20000006378, 'platShopId': '105765354', 'support': True},
    #     {'id': 10000305960, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_666660001',
    #      'thirdCaption': '数据平台测试店铺1', 'unitId': 20000006380, 'platShopId': '666660001', 'support': True},
    #     {'id': 10000305963, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_666660004',
    #      'thirdCaption': '数据平台组测试店铺4', 'unitId': 20000006383, 'platShopId': '666660004', 'support': True},
    #     {'id': 10000305964, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_666660005',
    #      'thirdCaption': '数据平台组测试店铺5', 'unitId': 20000006384, 'platShopId': '666660005', 'support': True},
    #     {'id': 10000306019, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_70582239',
    #      'thirdCaption': '店铺_mock', 'unitId': 20000006439, 'platShopId': '70582239', 'support': True},
    #     {'id': 10000306020, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_100094062',
    #      'thirdCaption': '店铺_mock', 'unitId': 20000006440, 'platShopId': '100094062', 'support': True},
    #     {'id': 10000306079, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_123456',
    #      'thirdCaption': '测试店铺', 'unitId': 20000006499, 'platShopId': '123456', 'support': True},
    #     {'id': 10000307333, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_145841584',
    #      'thirdCaption': '麦斯威尔旗舰店', 'unitId': 20000008364, 'platShopId': '145841584', 'support': True},
    #     {'id': 10000307363, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_2020061103',
    #      'thirdCaption': '模板引擎测试专用2', 'unitId': 20000008394, 'platShopId': '2020061103', 'support': True},
    #     {'id': 10000307367, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_2020061106',
    #      'thirdCaption': '模板引擎测试专用6', 'unitId': 20000008398, 'platShopId': '2020061106', 'support': True},
    #     {'id': 10000307374, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_66431452',
    #      'thirdCaption': '华院数云', 'unitId': 20000008405, 'platShopId': '66431452', 'support': True},
    #     {'id': 10000307379, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_2020061802',
    #      'thirdCaption': '历史数据处理061802', 'unitId': 20000008410, 'platShopId': '2020061802', 'support': True},
    #     {'id': 10000307403, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_100571094',
    #      'thirdCaption': '大狗子19890202', 'unitId': 20000008434, 'platShopId': '100571094', 'support': True},
    #     {'id': 10000307726, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_103179969',
    #      'thirdCaption': 'skechers童鞋旗舰店', 'unitId': 20000008865, 'platShopId': '103179969', 'support': True},
    #     {'id': 10000307727, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_109497705',
    #      'thirdCaption': 'skechers男鞋旗舰店', 'unitId': 20000008866, 'platShopId': '109497705', 'support': True},
    #     {'id': 10000307728, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_112900255',
    #      'thirdCaption': 'skechers运动旗舰店', 'unitId': 20000008868, 'platShopId': '112900255', 'support': True},
    #     {'id': 10000307729, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_69471491',
    #      'thirdCaption': 'skechers官方旗舰店', 'unitId': 20000008869, 'platShopId': '69471491', 'support': True},
    #     {'id': 10000307780, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_cjh_002',
    #      'thirdCaption': 'cjh淘宝店小二', 'unitId': 20000009011, 'platShopId': '87654567', 'support': True},
    #     {'id': 10000307781, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': '666888',
    #      'thirdCaption': '淘宝_cjh', 'unitId': 20000009012, 'platShopId': '666888', 'support': True},
    #     {'id': 10000307787, 'thirdType': 'PASSPORT', 'sourceType': 'TAOBAO', 'thirdId': 'taobao_666555',
    #      'thirdCaption': '淘宝非会员通账号', 'unitId': 20000009022, 'platShopId': '666555', 'support': True}]}]

    # print(Paramsplit().paramsplit(corr, results))
