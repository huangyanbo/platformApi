# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野
from interface.common.headers import Partner
from interface.excel.geturlParams import geturlParams
from common.log import Log
from interface.testCase.asserts.responseCodeCheck import ResponseCodeCheck
import requests, json
import common.info_error as excep

logger = Log()
url = geturlParams().get_Url()
mgurl = geturlParams().get_mgUrl()
rcc = ResponseCodeCheck()


class CheckResult:

    # 执行用例
    def checkresult(self, path_url, relation, head_token, case, project):  # 请求方法
        global info, infoText, infoCode, respTime
        try:
            param = case["case_request_data"]
            method = case["case_method"]
            data_format = case["case_format"]
            logger.info(f'请求方法：{method}')
            logger.info(f"请求路径：{path_url}")
            logger.info(f"请求参数：{param}")
            if method == 'GET':
                info = self.__get(param, path_url, relation, head_token)
            elif method == 'POST':
                info = self.__post(param, path_url, relation, head_token, data_format)
            elif method == 'PUT':
                info = self.__put(param, path_url, relation, head_token, data_format)
            elif method == 'DELETE':
                info = self.__delete(param, path_url, relation, head_token, data_format)

            respTime = info.elapsed.total_seconds()
            logger.info(f"响应时间：{respTime}")
            infoCode = info.status_code
            logger.info(f"响应状态：{infoCode}")
            infoText = info.text
            logger.info(f"响应结果：{infoText}")
            data = rcc.code_all(info, infoCode, respTime, case, project)
            return data, infoText
        except Exception as e:
            return excep.request_error_exception(e, respTime, infoCode)

    def __get(self, param, path_url, relation, head_token):
        if param:
            return requests.get(path_url, params=json.loads(param),
                                headers=Partner().patner_quota(path_url, relation, head_token))
        else:
            return requests.get(path_url, headers=Partner().patner_quota(path_url, relation, head_token))

    def __post_json(self, param, path_url, relation, head_token):
        return requests.post(path_url, json=json.loads(param),
                             headers=Partner().patner_quota(path_url, relation, head_token))

    def __post_form(self, param, path_url, relation, head_token):
        return requests.post(path_url, data=json.loads(param),
                             headers=Partner().patner_quota(path_url, relation, head_token))

    def __post_else(self, param, path_url, relation, head_token):
        params = param.encode('utf-8')
        return requests.post(path_url, data=params,
                             headers=Partner().patner_quota(path_url, relation, head_token))

    def __post(self, param, path_url, relation, head_token, data_format):
        if param:
            if data_format == "JSON":
                return self.__post_json(param, path_url, relation, head_token)
            elif data_format == "FORM":
                return self.__post_form(param, path_url, relation, head_token)
            else:
                return self.__post_else(param, path_url, relation, head_token)
        else:
            return requests.post(path_url, headers=Partner().patner_quota(path_url, relation, head_token))

    def __put(self, param, path_url, relation, head_token, data_format):
        if param:
            if data_format == "JSON":
                return requests.put(path_url, json=json.loads(param),
                                    headers=Partner().patner_quota(path_url, relation, head_token))
            elif data_format == "FORM":
                return requests.put(path_url, data=json.loads(param),
                                    headers=Partner().patner_quota(path_url, relation, head_token))
            else:
                params = param.encode('utf-8')
                return requests.put(path_url, data=params,
                                    headers=Partner().patner_quota(path_url, relation, head_token))
        else:
            return requests.put(path_url, headers=Partner().patner_quota(path_url, relation, head_token))

    def __delete(self, param, path_url, relation, head_token, data_format):
        if param:
            if data_format == "JSON":
                return requests.delete(path_url, json=json.loads(param),
                                       headers=Partner().patner_quota(path_url, relation, head_token))
            elif data_format == "FORM":
                return requests.delete(path_url, data=json.loads(param),
                                       headers=Partner().patner_quota(path_url, relation, head_token))
            else:
                return requests.delete(path_url, headers=Partner().patner_quota(path_url, relation, head_token))
        else:
            return requests.delete(path_url, headers=Partner().patner_quota(path_url, relation, head_token))
