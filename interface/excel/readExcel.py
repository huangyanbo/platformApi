# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野
import os
# 调用读Excel的第三方库xlrd
from xlrd import open_workbook
import configparser

# 拿到该项目所在的绝对路径
path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# 读取ini配置文件
config = configparser.ConfigParser()


class readExcel():
    def get_xls(self):  # xls_name填写用例的Excel名称 sheet_name该Excel的sheet名称
        cls = []
        files = os.listdir(path + '/testCase/test_file')
        for file in files:
            # print(file)
            # 获取用例文件路径
            xlsPath = os.path.join(path, 'testCase', "test_file", file)
            openFile = open_workbook(xlsPath)  # 打开用例Excel
            sheet_name = openFile.sheet_names()
            for sheetname in sheet_name:
                sheet = openFile.sheet_by_name(sheetname)  # 获得打开Excel的sheet
                # 获取这个sheet内容行数
                nrows = sheet.nrows
                for i in range(0, nrows):  # 根据行数做循环
                    # 如果这个Excel的这个sheet的第i行的第一列不等于非预期参数那么我们把这行的数据添加到cls[]
                    if sheet.row_values(i)[0] != u'编号' and sheet.row_values(i)[8] != 'No':
                        cls.append(sheet.row_values(i))
                    # print(sheet.row_values(i))
        return cls

    def get_xls_view(self):  # xls_name填写用例的Excel名称 sheet_name该Excel的sheet名称
        cls = []
        files = os.listdir(path + '/import_file')
        for file in files:
            # 获取用例文件路径
            xlsPath = os.path.join(path, "import_file", file)
            openFile = open_workbook(xlsPath)  # 打开用例Excel
            sheet_name = openFile.sheet_names()
            for sheetname in sheet_name:
                sheet = openFile.sheet_by_name(sheetname)  # 获得打开Excel的sheet
                # 获取这个sheet内容行数
                nrows = sheet.nrows
                for i in range(0, nrows):  # 根据行数做循环
                    # 如果这个Excel的这个sheet的第i行的第一列不等于非预期参数那么我们把这行的数据添加到cls[]
                    if sheet.row_values(i)[0] != u'编号' and sheet.row_values(i)[8] != 'No':
                        cls.append(sheet.row_values(i))
        return cls

    def read_excel_to_case(self, file_content):
        case_suite = open_workbook(file_contents=file_content)
        sheets = case_suite.sheet_names()  # 获取每个sheet名称
        case_list = []
        for sheet in sheets:
            sheet_content = case_suite.sheet_by_name(sheet)  # 获取sheet内容
            sheet_rows = sheet_content.nrows  # 获取sheet内容行数
            if sheet_rows > 2:  # 模板第一行忽略，第二行为标题行，正儿八经用例从第三行开始（秀）
                case_fields = [filed.strip() for filed in sheet_content.row_values(1)]  # 用例字段头去空格
                filed_index = self.case_index(case_fields)  # 获取sheet字段和用例规范的索引关系
                if filed_index["status"]:
                    for row in range(2, sheet_rows):
                        row_content = sheet_content.row_values(row)
                        case = {
                            'case_tag': sheet,
                            'case_id': row_content[filed_index['case_id']],
                            'case_name': row_content[filed_index['case_name']],
                            'case_path': row_content[filed_index['case_path']],
                            'case_url': row_content[filed_index['case_url']],
                            'case_method': row_content[filed_index['case_method']],
                            'case_headers': row_content[filed_index['case_headers']],
                            'case_format': row_content[filed_index['case_format']],
                            'case_request_data': row_content[filed_index['case_request_data']],
                            'case_check': row_content[filed_index['case_check']],
                            'case_relation_param': row_content[filed_index['case_relation_param']],
                            'template_name': row_content[filed_index['template_name']],
                            'template_param': row_content[filed_index['template_param']],
                            'template_check': row_content[filed_index['template_check']],
                            'template_relation_param': row_content[filed_index['template_relation_param']]

                        }  # 组装测试用例
                        # 用例组装完成之后需要进行用例的字段校验
                        # case_db_temp_name 需要配置且存在
                        # case_host 需要配置且存在
                        # case_name 需要sheet 唯一
                        # case_method 需要判断枚举存在

                        case_list.append(case)
                else:
                    return filed_index
        return case_list

    def case_index(self, case_fields):
        try:
            case_fields_index = {"status": True,
                                 'case_id': case_fields.index('用例编号'),
                                 'case_name': case_fields.index('用例名称'),
                                 'case_path': case_fields.index('接口域名'),
                                 'case_url': case_fields.index('接口地址'),
                                 'case_method': case_fields.index('请求方法'),
                                 'case_headers': case_fields.index('请求头信息'),
                                 'case_format': case_fields.index('参数类型'),
                                 'case_request_data': case_fields.index('请求参数'),
                                 'case_check': case_fields.index('响应断言'),
                                 'case_relation_param': case_fields.index('响应结果提取'),
                                 'template_name': case_fields.index('模板名称'),
                                 'template_param': case_fields.index('模板参数'),
                                 'template_check': case_fields.index('数据库断言'),
                                 'template_relation_param': case_fields.index('数据库参数提取')}
            return case_fields_index
        except Exception as e:
            return {"status": False, "msg": str(e)}