# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野

from interface.common.readConfig import ReadConfig
rc = ReadConfig()

# 定义一个方法，将从配置文件中读取的进行拼接
class geturlParams():

    # 通用的URL
    def get_Url(self):
        # new_url = rc.get_http('serviceUrl') + rc.get_http('apiUrl')
        new_url = rc.get_http('serviceUrl')
        return new_url

    # 普通mgURL
    def get_mgUrl(self):
        new_mgurl = rc.get_http('mgUrl')
        return new_mgurl

    # 淘宝url
    def get_tbUrl(self):
        new_tburl = rc.get_http('serviceUrl') + rc.get_http('taobaoHYTApiUrl')
        return new_tburl

    # 京东url
    def get_josUrl(self):
        new_josurl = rc.get_http('serviceUrl') + rc.get_http('josHYTApiUrl')
        return new_josurl

if __name__ == '__main__':
    print(geturlParams().get_Url())
    print(geturlParams().get_josUrl())
    print(geturlParams().get_tbUrl())
