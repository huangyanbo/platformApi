# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野

from flask import Blueprint
from interface.view.interfaceApi import interfaceTest
from interface.view.case_task import casetask
import interface.view.case_task as task
inter = interfaceTest()

interfacetest = Blueprint('interface', __name__, url_prefix='/test')

interfacetest.add_url_rule('/test_interface/', view_func=inter.interfaceApp,methods=['POST'])
interfacetest.add_url_rule('/HtmlDownload/', view_func=inter.download)
interfacetest.add_url_rule('/copyHtml/', view_func=inter.copyHtml)

interfacetest.add_url_rule('/fileUpload/', view_func=inter.fileUpload, methods=['POST'])
interfacetest.add_url_rule('/saveCase/', view_func=inter.save_case, methods=['POST'])  # 批量创建用例
interfacetest.add_url_rule('/uploadcase/', view_func=inter.uploadCase, methods=['POST'])  # 上传用例
interfacetest.add_url_rule('/caseStart/', view_func=inter.start_case, methods=['POST'])  # 执行用例

interfacetest.add_url_rule('/saveOneCase/', view_func=inter.saveOneCase, methods=['POST'])
# interfacetest.add_url_rule('/updateOneCase/', view_func=inter.updateOneCase, methods=['PUT'])  # 修改某条用例

interfacetest.add_url_rule('/getOneCase/', view_func=inter.get_one_case, methods=['GET'])  # 修改用例前弹窗数据查询
interfacetest.add_url_rule('/updateCaseInfo/', view_func=inter.update_case_info, methods=['PUT'])  # 修改某条用例
interfacetest.add_url_rule('/caseDelete/', view_func=inter.case_delete, methods=['DELETE'])  # 单条用例删除
interfacetest.add_url_rule('/caseMoreDelete/', view_func=inter.case_more_delete, methods=['DELETE'])  # 批量用例删除

interfacetest.add_url_rule('/readExcelCase/', view_func=inter.readExcelCase)

# 获取项目分支
interfacetest.add_url_rule('/selectBranch/', view_func=inter.selectAllBranch)
# 扫描项目路径
interfacetest.add_url_rule('/scanJava/', view_func=inter.scanJavaFile)

interfacetest.add_url_rule('/contrast', view_func=inter.contrast)

interfacetest.add_url_rule('/selectCaseAll/', view_func=inter.selectCaseall, methods=['POST'])  # 多选版本查询用例

# 查询用例执行记录
interfacetest.add_url_rule('/select_log/', view_func=inter.select_log, methods=['POST'])  # 多选版本查询用例

interfacetest.add_url_rule('/select_service_log/',view_func=inter.select_service_log, methods=['POST']) #根据服务查询版本
interfacetest.add_url_rule('/select_case_res_log/', view_func=inter.select_case_res_log)  # 多选版本查询用例


# 定时任务查询
interfacetest.add_url_rule('/selectTask/',view_func=casetask.select_task)
interfacetest.add_url_rule('/selectsv/',view_func=casetask.select_sv)
interfacetest.add_url_rule('/addTask/',view_func=casetask.add_task, methods=['POST'])
interfacetest.add_url_rule('/deleteTask/', view_func=casetask.delete_task) #删除任务

interfacetest.add_url_rule('/pause/',view_func=task.pausetask) #暂停
interfacetest.add_url_rule('/resume/',view_func=task.resumetask) #恢复
interfacetest.add_url_rule('/deletejob/',view_func=task.remove_task) #删除

interfacetest.add_url_rule('/startjob/',view_func=task.start_task) #启动

interfacetest.add_url_rule('/operationJob/', view_func=task.operation_case) #手动执行


