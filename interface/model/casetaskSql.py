# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野
class casetasksql:

    def select_task(self, project_name):
        '''
        查询定时任务
        :param project_id:
        :return:
        '''
        list = []
        sql = f"select * from task_time where project_name = '{project_name}'"
        list.append(sql)
        return list

    def select_sv(self, project_name):
        '''
        查询服务版本信息
        :return:
        '''
        list = []
        sql = f"select pv.service_id,ps.service_name,pv.id as version_id,pv.version_name from project_info pi, project_service ps,project_version pv where pi.id=ps.project_id and ps.id = pv.service_id and pi.project_name='{project_name}'"
        list.append(sql)
        return list

    def add_task(self, data):
        '''
        增加任务
        :return:
        '''
        list = []
        sql = f'''INSERT INTO `task_time`(`project_name`, `task_name`, `data_format`, `number`, `start_time`, 
        `end_time`, `task_status`, `remark`, `created`) VALUES ('{data["project_name"]}', '{data["task_name"]}', 
        '{data["data_format"]}', {data["number"]}, '{data['start_end'][0]}', '{data['start_end'][1]}', 0, '{data["remark"]}', NOW()); '''
        list.append(sql)
        return list

    def add_task_sv(self, service_version_id, task_id):
        '''
        增加任务服务版本
        :return:
        '''
        list = []
        for s1, v1 in service_version_id:
            sql = f'''INSERT INTO `task_data`(`service_id`, `version_id`, `task_id`) VALUES ({s1}, {v1}, {task_id});'''
            list.append(sql)
        return list

    def select_task_id(self, task_name):
        '''
        查询任务id
        :return:
        '''
        list = []
        sql = f'''select id from task_time where task_name="{task_name}";'''
        list.append(sql)
        return list

    def select_task_case(self, task_id):
        '''
        定时任务用例
        :param task_id:
        :return:
        '''
        list = []
        sql = f'''select pa.* from project_case pa,task_data td,task_time tt where pa.version_id = td.version_id and 
        td.task_id=tt.id and tt.id= {task_id}; '''
        list.append(sql)
        return list

    def update_task_status(self, task_id, status):
        list = []
        sql = f'''UPDATE `task_time` SET  `task_status` = {status} WHERE `id` = {task_id}; '''
        list.append(sql)
        return list
