# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野

'''
异常文件封装
'''


def request_error_exception(res, ret=None, recode=None):
    '''
    request异常抽象
    :param res: 实际响应结果
    :param ret: 响应时间
    :param recode: 响应状态码
    :return: 返回对应异常信息
    '''
    if ret is not None and recode is not None:
        return {"success": "false", "response": str(res), "responseTime": str(ret),
                "responseStatus": str(recode),"dbsuccess": "false", "dbresponse": None}, '{"success": "false"}'
    else:
        return {"success": "false", "response": str(res), "responseTime": str(0),
                "responseStatus": str(400),"dbsuccess": "false", "dbresponse": None}, '{"success": "false"}'


def assert_err_exception(info, respTime, infocode):
    '''
    断言失败返回
    :param info: 响应结果
    :param respTime: 响应时间
    :param infocode: 响应状态
    :return:
    '''
    return {"success": "false", "response": str(info), "responseTime": str(respTime),
            'responseStatus': str(infocode), "dbsuccess": "false", "dbresponse": "None"}


def db_assert_err_exception(info):
    '''
    断言失败返回
    :param info: 响应结果
    :param respTime: 响应时间
    :param infocode: 响应状态
    :return:
    '''
    return {"dbsuccess": "false", "dbresponse": str(info)}


def db_check_err(info):
    '''
    用例定义检查点异常
    :param info:
    :return:
    '''
    return {"dbsuccess": "false", "dbresponse": "用例自定义预期异常:" + str(info)}


def assert_info(info, respTime, infocode):
    '''
    断言成功返回
    :param info:
    :param respTime:
    :param infocode:
    :return:
    '''
    return {"success": "true", "response": str(info), "responseTime": str(respTime),
            'responseStatus': str(infocode)}


def db_assert_info(info):
    '''
    断言成功返回
    :param info:
    :return:
    '''
    return {"dbsuccess": "true", "dbresponse": str(info)}


def case_check_error():
    return {"dbsuccess": "false", "dbresponse": "预期结果未定义"}


def db_select_error():
    return {"dbsuccess": "false", "dbresponse": "库查询结果为空"}


def db_select_null(db_info, sql_check):
    if str(db_info) == sql_check:
        return {"dbsuccess": "true", "dbresponse": str(db_info)}
    else:
        return {"dbsuccess": "false", "dbresponse": str(db_info)}


def db_param_null():
    return {"dbsuccess": "true", "dbresponse": "此用例无SQL检查"}

def db_start_true():
    return {"dbsuccess": "true", "dbresponse": "数据处理完成"}

def case_result_error():
    return {"dbsuccess": "false", "dbresponse": "未执行到此，为啥？心里没点数吗，去排查下接口用例吧！"}


# 接口成功到数据库异常
def case_db_error(exce):
    return {"dbsuccess": "false", "dbresponse": str(exce)}
