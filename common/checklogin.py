#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2021/5/6 11:38
@Author  : jianhua.cheng
'''
from flask import session,jsonify

def check_login(func):
    def inner(*args, **kwargs):
        if session.get('user'):
            ret = func(*args, **kwargs)
            return ret
        else:
            return jsonify({"code":-1,"msg":"session失效,请重新登录"})
    return inner