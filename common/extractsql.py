# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野

import re


class ExtractSql:
    def extract_table_name_from_sql(self, sql):
        q = re.sub(r"/\*[^*]*\*+(?:[^*/][^*]*\*+)*/", "", str(sql))
        lines = [line for line in q.splitlines() if not re.match("^\s*(--|#)", line)]
        q = " ".join([re.split("--|#", line)[0] for line in lines])
        tokens = re.split(r"[\s)(;]+", q)
        result = []
        get_next = False
        for token in tokens:
            if get_next:
                if token.lower() not in ["", "select"]:
                    result.append(token)
            # get_next = token.lower() in ["from", "join"]
            get_next = token.lower() in ["from"]
        return result


# if __name__ == '__main__':
#     sql = 'select tb.mobile_key,tm.customer_id,tb.plat_dp_name,tb.shop_id,tb.tenant_id,tb.plat_code                   ' \
#           '  from t_m_mem_brand tb join t_r_shopid_mapping tm on tb.shop_id = tm.shop_id                     where ' \
#           'tb.plat_dp_name = "CLASSY·KISS卡士旗舰店" and tm.plat_code="JOS" and tb.tenant_id = "qiushi6" '
#
#     print(ExtractSql().extract_table_name_from_sql(sql))
