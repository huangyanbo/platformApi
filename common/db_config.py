# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野

import configparser
import os
import pymysql

path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
config = configparser.ConfigParser()
config.read(path + '/config/config.ini', encoding='utf-8')


class DB_Config(object):


    def db_conn2_prod(self, dbURL, user, password, database):
        return pymysql.connect(host="{}".format(dbURL),
                               port=3306,
                               user="{}".format(user),
                               password="{}".format(password),
                               database="{}".format(database),
                               charset='utf8',
                               cursorclass=pymysql.cursors.DictCursor)


    def db_test_platform(self):
        return pymysql.connect(host='localhost',
                               port=3306,
                               user="root",
                               password="",
                               database="platform_test",
                               cursorclass=pymysql.cursors.DictCursor)  # 本地测试使用


    def db_common(self, data):
        return pymysql.connect(host=data["db_host"],
                               port=data["db_port"],
                               user=data["db_username"],
                               password=data["db_password"],
                               database=data["db_name"],
                               charset='utf8',
                               cursorclass=pymysql.cursors.DictCursor)

#
# if __name__ == '__main__':
#     # DB_Config().table_name('all')
#     print(DB_Config().db_conn_prod(env="prod",data=None))
