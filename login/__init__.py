from flask import Blueprint
import login.login as login


loginng = Blueprint('login', __name__, url_prefix='/test')


loginng.add_url_rule('/login/', view_func=login.login, methods=['POST'])
loginng.add_url_rule('/logout/', view_func=login.logout, methods=['POST'])
loginng.add_url_rule('/getsession/<key>/', view_func=login.get_session_value, methods=['GET'])
loginng.add_url_rule('/regist/', view_func=login.regist, methods=['POST'])