# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野

from flask import Blueprint
from project_setting.view.settingApi import SettingApi
from project_setting.view import dbConfig as dc
stapi = SettingApi()

project_setting = Blueprint('project_setting', __name__, url_prefix='/test')
#新增项目
project_setting.add_url_rule('/addProject/', view_func=stapi.addProject, methods=['PUT'])
project_setting.add_url_rule('/addVersion/', view_func=stapi.addVersion, methods=['PUT'])
project_setting.add_url_rule('/addService/', view_func=stapi.addService, methods=['PUT'])
# 更新项目
project_setting.add_url_rule('/updateProject/', view_func=stapi.updateProject, methods=['PUT'])
project_setting.add_url_rule('/updateService/', view_func=stapi.updateService, methods=['PUT'])
project_setting.add_url_rule('/updateVersion/', view_func=stapi.update_version, methods=['PUT'])
project_setting.add_url_rule('/addHost/', view_func=stapi.add_host, methods=['POST'])
project_setting.add_url_rule('/select_jenkins_log/', view_func=stapi.select_jenkins_log)

# 查询项目
project_setting.add_url_rule('/selectProject/', view_func=stapi.selectProject)
# 通过项目id获取对应的服务
project_setting.add_url_rule('/serviceVersion/', view_func=stapi.serviceVersion)
# 通过项目id查询项目数据
project_setting.add_url_rule('/select_project_id/', view_func=stapi.select_project_id)
project_setting.add_url_rule('/select_service_id/', view_func=stapi.select_service_id)
# 通过服务id获取对应的版本号
project_setting.add_url_rule('/select_version/', view_func=stapi.select_version)
project_setting.add_url_rule('/select_version_id/', view_func=stapi.select_version_id)
# 查询host配置
project_setting.add_url_rule('/select_host/', view_func=stapi.select_host)
# 删除host配置
project_setting.add_url_rule('/delete_url/', view_func=stapi.delete_url, methods=['DELETE'])

# 查询数据库配置
project_setting.add_url_rule('/get/dbconfig',view_func=dc.getDbConfig)
project_setting.add_url_rule('/add/dbconfig',view_func=dc.addDbConfig,methods=['POST'])

