# !/usr/bin/env python
# -*- encoding: utf-8 -*-

def select_db_config():
    """查询数据配置信息"""
    sqls = []
    sql = """SELECT * FROM project_info pi INNER  JOIN db_config dc ON pi.id=dc.project_id"""
    sqls.append(sql)
    return sqls

def add_db_config(data):
    sqls = []
    sql = f"""INSERT INTO `db_config`(`dbname`, `dbhost`, `dbusername`, `dbpassword`, `project_id`, `db_env`, 
    `dbport`, `dbdatabase`) VALUES ( '{data['dbname']}', '{data['dbhost']}', '{data['dbusername']}', 
    '{data['dbpassword']}', {data['project_id']}, '{data['db_env']}', {data['dbport']}, '{data['dbdatabase']}'); """
    sqls.append(sql)
    return sqls
