# !/usr/bin/env python
# -*- encoding: utf-8 -*-
from flask import request,jsonify
from project_setting.model import dbConfigSql as ds
from common.database import DataBase
import json
db = DataBase()
def getDbConfig():
    sql = ds.select_db_config()
    res = db.test_platform(sql)
    return jsonify(res)

def addDbConfig():
    data = json.loads(request.data)
    sql = ds.add_db_config(data)
    res = db.test_platform(sql,m='w')
    return jsonify(res)