# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# @Author : 大野
from project_setting.model.settingSql import SettingSql
import json
from flask import request, jsonify
from common.database import DataBase

db = DataBase()
ss = SettingSql()


class SettingApi:
    def addProject(self):
        """增加项目api"""
        data = json.loads(request.data)
        sql = ss.add_project(data['project_name'], data['project_code'], data['project_manager'], data['desc'])
        res = db.test_platform(sql, m='w')
        return jsonify(res)

    def addService(self):
        '''
        增加服务api
        :return:
        '''
        data = json.loads(request.data)
        sql = ss.add_service(data['project_id'], data['service_name'], data['service_desc'])
        res = db.test_platform(sql, m='w')
        return jsonify(res)

    def addVersion(self):
        '''
        增加服务版本api
        :return:
        '''
        data = json.loads(request.data)
        # serviceIdSql = ss.select_serviceId(data["service"])
        # serviceId = db.test_platform(serviceIdSql)
        sql = ss.add_version(data["project_id"], data["service_id"], data["version_name"], data['version_manage'],
                             data['version_desc'])
        db.test_platform(sql, m='w')
        return jsonify({"success": "true"})

    def selectProject(self):
        '''
        查询项目
        :return:
        '''
        sql = ss.select_project()
        project = db.test_platform(sql)
        return jsonify(project)

    def serviceVersion(self):
        '''
        查询服务版本
        :return:
        '''
        project_id = request.args.get('project_id')
        sql = ss.select_service(project_id)
        data = db.test_platform(sql)
        return jsonify(data)

    def updateProject(self):
        '''
        更新项目
        :return:
        '''
        data = json.loads(request.data)
        sql = ss.updeta_project(data['project_name'], data['project_code'], data['project_manager'], data['desc'],
                                data['project_id'])
        try:
            db.test_platform(sql, m='w')
            return jsonify({"success": "true"})
        except Exception as e:
            return jsonify({"success": "false","msg":str(e)})

    def select_project_id(self):
        project_id = request.args.get("project_id")
        sql = ss.select_project_id(project_id)
        data = db.test_platform(sql)
        return jsonify(data)

    def select_service_id(self):
        """通过服务id查询服务信息"""
        service_id = request.args.get("service_id")
        sql = ss.select_service_id(service_id)
        data = db.test_platform(sql)
        return jsonify(data)

    def select_version_id(self):
        """通过版本id查询版本信息"""
        version_id = request.args.get("version_id")
        sql = ss.select_version_id(version_id)
        data = db.test_platform(sql)
        return jsonify(data)

    def updateService(self):
        '''
        更新服务
        :return:
        '''
        data = json.loads(request.data)
        sql = ss.updeta_service(data['service_name'], data['service_desc'], data['service_id'])
        try:
            db.test_platform(sql, m='w')
            return jsonify({"success": "true"})
        except Exception as e:
            return jsonify({"success": "false"})

    def select_version(self):
        """查询版本号"""
        service_id = request.args.get("service_id")
        sql = ss.select_version(service_id)
        data = db.test_platform(sql)
        return jsonify(data)

    def update_version(self):
        """修改版本号"""
        data = json.loads(request.data)
        sql = ss.update_version(data['version_name'], data['version_manage'], data['version_desc'], data['version_id'])
        try:
            db.test_platform(sql, m='w')
            return jsonify({"success": "true"})
        except Exception as e:
            return jsonify({"success": "false"})

    def add_host(self):
        """新增url映射路径"""
        data = json.loads(request.data)
        sql = ss.add_host(data)
        resp = db.test_platform(sql, m='w')
        return resp

    def select_host(self):
        """查询url映射路径"""
        project_id = request.args.get("project_id")
        sql = ss.select_host(project_id)
        resp = db.test_platform(sql)
        return jsonify(resp)

    def select_jenkins_log(self):
        """项目jenkins构建执行日志"""
        from common.excuteJenkinsJob import ExcuteJenkinsJob
        job = ExcuteJenkinsJob()
        result = job.jenkinsLog("test_platform_api")
        return jsonify({"result": result})

    def delete_url(self):
        """删除host"""
        id = request.args.get("id")
        sql = ss.delete_url_info(id)
        res = db.test_platform(sql,m='w')
        return jsonify(res)