### 访问地址（登录vpn）：https://test-platform.xxxxxx.com/#/

### 登录账号：使用公司个人账号登录

**项目结构**
> JENKINS库安装命令： pip install python-jenkins
> YAML库安装：pip install pyyaml
```
│   manage.py
│   README.md
│   requirements.txt
│   start.sh
│           
├───common
│   │   created_phone.py
│   │   cryptTool.py
│   │   database.py
│   │   date_time.py
│   │   db_config.py
│   │   extractsql.py
│   │   HTMLTestRunner.py
│   │   log.py
│   │   __init__.py
│           
├───config
│       config.ini
│       __init__.py
│       
├───interface
│   │   __init__.py
│   │   
│   ├───common
│   │   │   configEmail.py
│   │   │   cryptTool.py
│   │   │   headers.py
│   │   │   HttpRequests.py
│   │   │   logger.py
│   │   │   paramsplit.py
│   │   │   paramunittest.py
│   │   │   readConfig.py
│   │   │   resultchange.py
│   │   │   SQL.py
│   │   │   __init__.py
│   │   │   
│   ├───config
│   │   │   callerandsecret.ini
│   │   │   caselist.txt
│   │   │   config.ini
│   │   │   cookie.ini
│   │   │   __init__.py
│   │   │   
│   │   └───__pycache__
│   │           __init__.cpython-37.pyc
│   │           
│   ├───excel
│   │   │   geturlParams.py
│   │   │   readExcel.py
│   │   │   __init__.py
│   │           
│   ├───import_file
│   │       testcasemock.xlsx
│   │       
│   ├───model
│   │   │   selectSql.py
│   │   │   __init__.py
│   │           
│   ├───resultReport
│   │   │   __init__.py
│   │   │   
│   │   ├───HtmlReport
│   │   │       接口自动化测试报告.html
│   │   │       
│   │   ├───logs
│   │           
│   ├───start
│   │   │   runAll.py
│   │   │   __init__.py
│   │     
│   ├───testCase
│   │   │   test.py
│   │   │   test01case.py
│   │   │   test_test.py
│   │   │   
│   │   ├───asserts
│   │   │   │   checkResult.py
│   │   │   │   responseCodeCheck.py
│   │   │   │   __init__.py
│   │   │           
│   │   ├───test_file
│   │   │       case.txt
│   │           
│   ├───view
│   │   │   interfaceApi.py
│   │   │   __init__.py
│           
├───testDataClear
│   │   __init__.py
│   │   
│   ├───config
│   │       config.ini
│   │       __init__.py
│   │       
│   ├───model
│   │   │   delete_data.py
│   │   │   select_data.py
│   │   │   __init__.py
│   │           
│   ├───view
│   │   │   viewmodel.py
│   │   │   __init__.py
│           
├───testToolsClass
│   │   __init__.py
│   │   
│   ├───model
│   │   │   select_data.py
│   │   │   select_member.py
│   │   │   __init__.py
│   │           
│   ├───view
│   │   │   encry_decry.py
│   │   │   mock_data.py
│   │   │   viewmodel.py
│   │   │   __init__.py
│   │   │   
│   │   ├───test
│           
├───report
│   │   __init__.py
│   │   
│   ├───data_file
│   │       testData20201030165306.txt
│   │       
│   └───log
│           
├───templates
├───tools
│       crypt-1.0.9.jar
│       crypt-util-2.0.jar
│       __init__.py
│       
├───venv
```



###   **功能描述**  

数据管理

​	1.会员数据管理

​	2.卡数据管理

​	3.店铺数据管理（待开放）

工具管理

​		1.淘宝、京东密文手机号生成

​		2.数云账号手机号加密

自动化

​		1.服务版本管理

​		2.用例管理

​		3.用例执行



### **自动化用例编写规范说明**(如附件图)

A.编号

```
每个用例excel文本里面的id必须唯一，包含多个sheet
```

B.接口名称

```
根据接口业务自定义名称
```

C.地址前缀		

```
当前test地址前缀有以下类型：

​ taobaoHYTApiUrl、qiakrApiUrl、mgUrl、josHYTApiUrl、serviceUrl

​ 淘宝相关接口用：taobaoHYTApiUrl

​ 京东相关接口用：josHYTApiUrl

​ 线下相关接口用：qiakrApiUrl

​ 和前端交互的接口用：mgUrl

​ 其他接口用：serviceUrl

​ 前缀和域名映射关系：

​   mgUrl=https://qa-ual.xxxxxx.com

​   serviceUrl=http://qa-sapi.xxxxxx.com（taobaoHYTApiUrl、qiakrApiUrl、josHYTApiUrl、serviceUrl）
```

D.请求地址		

```
服务名称+'/'+版本号+'/'+子路径

比如：loyalty2-interface/1.0/spi/members/register
```

E.请求方法（目前支持）

```
POST、GET、PUT
```

F.请求数据类型  JSON

G.请求数据

> 必须是json格式
>
> 	比如：
> 	{ 
> 	"seller_name":"奥康国际官方旗舰店",
> 	"mix_mobile":"91cb95d490f5277997799f3bf5c7a642",
> 	"taobao_nick":"TAOBAO自动化数据"
> 	}

H.检查点

```
必须是json格式或者接口响应码，json格式里面的键值对必须是唯一
	比如：{"register_code": "SUC"}
如果实际响应结果里面有多层嵌套，可以通过提取唯一键值对放在检查点里
	比如：实际响应结果{a:[b:b1,c:c1,d:[e:e1,f:f1]]}
	 	检查写法：{b:b1,c:c1,f:f1}
```

I.是否运行

```
Yes	运行此用例
No	不执行此用例
```

J.关联参数拆分结果:如下图

```html
变量写法: $[x]=['x']
使用变量写法：${x | 用例ID}
```

K.参数关联编号

